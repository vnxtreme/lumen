<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scraped_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data_id', '255')->nullable();
            $table->string('title', '255')->nullable();
            $table->string('href', '255')->nullable();
            $table->text('excerpt')->nullable();
            $table->text('body')->nullable();
            $table->string('category', '255')->nullable();
            $table->string('thumbnail', '255')->nullable();
            $table->string('images', '255')->nullable();
            $table->string('post_time', '255')->nullable();
            $table->string('source', '255')->nullable();
            $table->smallInteger('hot')->unsigned()->default(0);
            $table->smallInteger('feature')->unsigned()->default(0);
            $table->smallInteger('video_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scraped_data');
    }
}
