<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class ConvertToPostFormatController extends Controller
{
    public function index()
    {
        $scrapedData = DB::table('scraped_data')->get();
        $posts = DB::table('posts');

        foreach ($scrapedData as $data) {
            DB::table('posts')->insert([
                'author_id' => 1,
                'title' => $data->title,
                'seo_title' => $data->title,
                'excerpt' => $data->excerpt,
                'slug' => $data->slug,
                'image' => $data->thumbnail,
                'facebook_image' => $data->thumbnail,
                'main_content' => $data->body,
                'parent_category' => rand(1,4),
                'publish_time' => $data->post_time,
                'status' => 'PUBLISHED'
            ]);
        }
    }
}
