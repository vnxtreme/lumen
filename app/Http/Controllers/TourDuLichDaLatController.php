<?php

namespace App\Http\Controllers;

use App\ScrapedData;
use App\Traits\Slugify;
use Goutte\Client;
use Illuminate\Support\Facades\DB;

class TourDuLichDaLatController extends Controller
{
    use Slugify;

    private $client;
    protected $url = 'https://toursdulichdalat.com/thong-tin-du-lich/';
    protected $imagePath = 'images\\';
    protected $rootPath  = '/storage/'; // for images of admin website
    protected $videoPath = 'https://toursdulichdalat.com';// 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function index()
    {
        echo "<div style='text-align:center; font-size: 2em;'>{$this->getPosts($this->url)} new post(s)</div>";
    }

    /**
     * Scrape and return total of inserted record(s)
     *
     * @return int
     */
    public function getPosts(String $url)
    {
        $listNews    = [];
        $scrapeNodes = $this->scrapeThisUrl($url);

        $allClassInfo = $this->getClassInfo($url);
        if (!$allClassInfo):
            throw new \Exception("Chưa cấu hình Class", 1);
        endif;

        foreach ($allClassInfo as $classInfo):
            $listNews += $this->getListNews($scrapeNodes, $classInfo);
        endforeach;

        return count(array_filter($listNews));
    }

    /**
     * Library method to scrape sites
     *
     * @param String $url
     * @return object
     */
    protected function scrapeThisUrl(String $url)
    {
        return $this->client->request('GET', $url);
    }


    /**
     * List news
     *
     * @param object $scrapeNodes
     * @param array $images
     * @param array $data
     * @return array only db-inserted object
     */
    public function getListNews(Object $scrapeNodes, Object $classInfo, array $images = [], array $data = [])
    {
        return $scrapeNodes->filter($classInfo->wrapper)->each(function ($node) use ($images, $data, $classInfo) {
            $dataIdElement = $node->filter($classInfo->data_id);
            if($dataIdElement->count() and $dataIdElement->attr('id') != 'post-6079')://check if class exist
                $dataID = $dataIdElement->attr('id');
                
                $exist  = $this->isExist($dataID);
                if ($exist):
                    return false;
                endif;

                $title        = $node->filter($classInfo->title)->html();
                $href         = $node->filter($classInfo->href)->attr('href');
                $thumbnailURL = $node->filter($classInfo->thumbnail)->attr('data-src');
                
                $this->saveToStorage($thumbnailURL, 'images');

                $detailNodes  = $this->scrapeThisUrl($href);
                $contentNodes = $detailNodes->filter($classInfo->body);
                
                $bodyHtml     = $contentNodes->html();
                $bodyHtml     = $this->replaceHTMLimageURL($detailNodes, $bodyHtml);

                $images += $this->getContentImages($detailNodes, $classInfo->body);

                if ($contentNodes->filterXPath($classInfo->video)->getNode(0)):
                    $videoFirstURL = $contentNodes->filterXPath($classInfo->video)->attr('data-vid');
                    $videoURL      = "{$this->videoPath}/{$videoFirstURL}";

                    $limit = ini_get('memory_limit'); //get current limit
                    ini_set('memory_limit', -1); //unset limit
                    $videoName = $this->saveToStorage($videoURL, 'videos');
                    ini_set('memory_limit', $limit); //reset to current limit
                endif;
        
                $data = [
                    // ['data-id'   => $dataID],
                    'href'      => str_replace_first('/', '', $href),
                    'title'     => $title,
                    'slug'      => $this->slugify($title),
                    'thumbnail' => $this->imagePath . $this->getFileNameFromURL($thumbnailURL),
                    'data-id'   => $dataID,
                    'category'  => 'Thông tin du lịch Đà Lạt',
                    'post_time' => /* $detailNodes->filter($classInfo->post_time)->text() */(new \DateTime('now'))->format('Y-m-d'),
                    'excerpt'   => $node->filter($classInfo->excerpt)->text(),
                    'body'      => $bodyHtml,
                    'images'    => json_encode($images),
                    'source'    => $node->getUri(),
                    'video'     => isset($videoName) ? "videos/{$videoName}" : null
                ];

                return (ScrapedData::firstOrCreate($data))->id;
            endif;
        });
    }

    /**
     * All images in detail post
     *
     * @param object $detailNodes
     * @return array $imagePath + $imageName
     */
    protected function getContentImages(Object $detailNodes, $classBody)
    {
        return $detailNodes->filter($classBody.' img')->each(function ($imageNode) {
            $imageUrl = $imageNode->attr('src');
            
            if(@getimagesize($imageUrl) and @getimagesize($imageUrl)['mime'] != 'image/gif'){
                $this->saveToStorage($imageUrl, 'images');
                $imageName = $this->getFileNameFromURL($imageUrl);
    
                return $this->imagePath . $imageName;
            }
        });
    }

    /**
     * Save image to storage then return saved name
     * Config in bootstrap/app.php
     *
     * @param String $url
     * @return void
     */
    protected function saveToStorage(String $url, String $folder)
    {
        $storage  = app('filesystem');
        $contents = file_get_contents($url);
        $name     = $this->getFileNameFromURL($url);
        $storage->put("{$folder}/{$name}", $contents);
        return $name;
    }

    protected function getFileNameFromURL(String $url)
    {
        return substr($url, strrpos($url, '/') + 1);
    }

    protected function replaceHTMLimageURL(Object $detailNodes, String $bodyHtml)
    {
        $arrayOriginalImageURL = $detailNodes->filter('img')->each(function ($imageNode) {
            return $imageNode->attr('src');
        });

        foreach ($arrayOriginalImageURL as $imageURL):
            $newImageUrl = "{$this->rootPath}/images/{$this->getFileNameFromURL($imageURL)}";
            $bodyHtml    = str_replace($imageURL, $newImageUrl, $bodyHtml);
        endforeach;

        return $bodyHtml;
    }

    protected function isExist($dataID)
    {
        return ScrapedData::where('data-id', $dataID)->exists();
    }

    protected function getClassInfo(String $url)
    {
        return app('db')->table('scrapers')->where('site', $url)->get();
    }

    // /**
    //  * Only apply for Body content HTML
    //  *
    //  * @param Object $detailNodes
    //  * @return string
    //  */
    // public function removeBodyHTMLLastLink(Object $contentNodes)
    // {
    //     $arrayBodyHtml = $contentNodes->children()->each(function ($node) {
    //         if (strpos($node->attr('class'), 'link-content-footer')):
    //             return false;
    //         endif;

    //         return $node->html();
    //     });

    //     return join("<br/>", $arrayBodyHtml);
    // }
    // /**
    //  * 1 feature news
    //  *
    //  * @param object $scrapeNodes
    //  * @param array $images
    //  * @return array only db-inserted object
    //  */
    // private function getFeatureNews(Object $scrapeNodes, $images = [])
    // {
    //     $dataID = $scrapeNodes->filter('.klw-featured-news .klwfn-left .gfn-postion .gfnp-title a')->attr('data-id');
    //     $exist  = $this->isExist($dataID);

    //     if ($exist):
    //         return null;
    //     endif;

    //     $postDetailHref = $scrapeNodes->filter('.klw-featured-news .klwfn-left .gfn-postion a')->attr('href');
    //     $detailNodes    = $this->scrapeThisUrl($postDetailHref);
    //     $thumbnailURL   = $scrapeNodes->filter('.klw-featured-news .klwfn-left .gfn-postion a img')->attr('src');
    //     $contentNodes   = $detailNodes->filter('.klw-new-content .fr .knc-content');
    //     // $bodyHtml       = $this->removeBodyHTMLLastLink($contentNodes);
    //     $bodyHtml = $contentNodes->html();
    //     $bodyHtml = $this->replaceHTMLimageURL($detailNodes, $bodyHtml);
    //     $title    = $scrapeNodes->filter('.klw-featured-news .klwfn-left .gfn-postion a')->attr('title');
    //     $images += $this->getContentImages($detailNodes);
    //     $this->saveToStorage($thumbnailURL, 'images');

    //     $videoURL = null;
    //     if ($contentNodes->filterXPath('//div[@data-vid]')->getNode(0)):
    //         $videoFirstURL = $contentNodes->filterXPath('//div[@data-vid]')->attr('data-vid');
    //         $videoURL      = "{$this->videoPath}/{$videoFirstURL}";

    //         $limit = ini_get('memory_limit');
    //         ini_set('memory_limit', -1);
    //         $videoName = $this->saveToStorage($videoURL, 'videos');
    //         ini_set('memory_limit', $limit);
    //     endif;

    //     $data = [
    //         'href'      => str_replace_first('/', '', $postDetailHref),
    //         'title'     => $title,
    //         'slug'      => $this->slugify($title),
    //         'thumbnail' => $this->imagePath . $this->getFileNameFromURL($thumbnailURL),
    //         'data-id'   => $dataID,
    //         'category'  => null,
    //         'post_time' => (new \DateTime)->format('Y-m-d H:i:s'),
    //         'excerpt'   => $scrapeNodes->filter('.klw-featured-news .klwfn-left .gfn-postion .klwfnl-sapo')->text(),
    //         'body'      => $bodyHtml,
    //         'images'    => json_encode($images),
    //         'feature'   => 1,
    //         'source'    => 'genk',
    //         'video'     => isset($videoName) ? "videos/{$videoName}" : null,
    //     ];

    //     return ScrapedData::firstOrCreate($data);
    // }

    // /**
    //  * 1 sub feature news
    //  *
    //  * @param object $scrapeNodes
    //  * @param array $images
    //  * @return array only db-inserted object
    //  */
    // private function getSubFeatureNews(Object $scrapeNodes, $images = [])
    // {
    //     $dataID = $scrapeNodes->filter('.klw-featured-news .klwfn-right .gfn-postion .klwfnr-title a')->attr('data-id');
    //     $exist  = $this->isExist($dataID);

    //     if ($exist):
    //         return null;
    //     endif;

    //     $postDetailHref = $scrapeNodes->filter('.klw-featured-news .klwfn-right .gfn-postion a')->attr('href');
    //     $detailNodes    = $this->scrapeThisUrl($postDetailHref);
    //     $thumbnailURL   = $scrapeNodes->filter('.klw-featured-news .klwfn-right .gfn-postion a img')->attr('src');
    //     $contentNodes   = $detailNodes->filter('.klw-new-content .fr .knc-content');
    //     // $bodyHtml       = $this->removeBodyHTMLLastLink($contentNodes);
    //     $bodyHtml = $contentNodes->html();
    //     $bodyHtml = $this->replaceHTMLimageURL($detailNodes, $bodyHtml);
    //     $title    = $scrapeNodes->filter('.klw-featured-news .klwfn-right .gfn-postion a')->attr('title');
    //     $images += $this->getContentImages($detailNodes);
    //     $this->saveToStorage($thumbnailURL, 'images');

    //     $videoURL = null;
    //     if ($contentNodes->filterXPath('//div[@data-vid]')->getNode(0)):
    //         $videoFirstURL = $contentNodes->filterXPath('//div[@data-vid]')->attr('data-vid');
    //         $videoURL      = "{$this->videoPath}/{$videoFirstURL}";

    //         $limit = ini_get('memory_limit');
    //         ini_set('memory_limit', -1);
    //         $videoName = $this->saveToStorage($videoURL, 'videos');
    //         ini_set('memory_limit', $limit);
    //     endif;

    //     $data = [
    //         'href'      => str_replace_first('/', '', $postDetailHref),
    //         'title'     => $title,
    //         'slug'      => $this->slugify($title),
    //         'thumbnail' => $this->imagePath . $this->getFileNameFromURL($thumbnailURL),
    //         'data-id'   => $dataID,
    //         'category'  => null,
    //         'post_time' => (new \DateTime)->format('Y-m-d H:i:s'),
    //         'excerpt'   => $detailNodes->filter('.klw-new-content .fr .knc-sapo')->text(),
    //         'body'      => $bodyHtml,
    //         'images'    => json_encode($images),
    //         'feature'   => 1,
    //         'source'    => 'genk',
    //         'video'     => isset($videoName) ? "videos/{$videoName}" : null,
    //     ];

    //     return ScrapedData::firstOrCreate($data);
    // }
}
