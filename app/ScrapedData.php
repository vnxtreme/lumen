<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScrapedData extends Model
{
    protected $table = "scraped_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            if($model->post_time):
                $date = new \DateTime($model->post_time);
                $model->post_time = $date->format('Y-m-d H:i:s');
            endif;
        });
    }
}
